#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();
	public slots:
	void setValue(int);
	void setValue1(int);
	void setValue2(int);
private:
    Ui::MainWindow *ui;
};

#endif // MAINWINDOW_H
