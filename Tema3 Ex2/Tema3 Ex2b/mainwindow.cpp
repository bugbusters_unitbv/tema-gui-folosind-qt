#include "mainwindow.h"
#include "ui_mainwindow.h"
#include<qpalette.h>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
	connect(ui->spinBox, SIGNAL(valueChanged(int)), ui->verticalSlider, SLOT(setValue(int)));
	connect(ui->spinBox_2, SIGNAL(valueChanged(int)), ui->verticalSlider_2, SLOT(setValue(int)));
	connect(ui->spinBox_3, SIGNAL(valueChanged(int)), ui->verticalSlider_3, SLOT(setValue(int)));
	connect(ui->verticalSlider, SIGNAL(valueChanged(int)), ui->spinBox, SLOT(setValue(int)));
	connect(ui->verticalSlider_2, SIGNAL(valueChanged(int)), ui->spinBox_2, SLOT(setValue(int)));
	connect(ui->verticalSlider_3, SIGNAL(valueChanged(int)), ui->spinBox_3, SLOT(setValue(int)));
	connect(ui->verticalSlider, SIGNAL(valueChanged(int)), this, SLOT(setValue(int)));
	connect(ui->verticalSlider_2, SIGNAL(valueChanged(int)), this, SLOT(setValue1(int)));
	connect(ui->verticalSlider_3, SIGNAL(valueChanged(int)), this, SLOT(setValue2(int)));
	
	
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::setValue(int)
{

	QPalette pal = palette();
	pal.setColor(QPalette::Background, Qt::red);
	setAutoFillBackground(true);
	setPalette(pal);
}

void MainWindow::setValue1(int i)
{
	QPalette pal = palette();
	pal.setColor(QPalette::Background, Qt::green);
	setAutoFillBackground(true);
	setPalette(pal);
}

void MainWindow::setValue2(int)
{
	QPalette pal = palette();
	pal.setColor(QPalette::Background, Qt::blue);
	setAutoFillBackground(true);
	setPalette(pal);
}


