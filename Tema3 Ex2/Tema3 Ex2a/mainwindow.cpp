#include "mainwindow.h"
#include "ui_mainwindow.h"
#include<qmessagebox.h>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
	connect(ui->pushButton, SIGNAL(clicked()), this, SLOT(pushbutton()));
}

MainWindow::~MainWindow()
{
    delete ui;
}
void MainWindow::pushbutton()
{
	
	ui->lineEdit_2->setText(ui->lineEdit->text());
	QMessageBox::information(this, tr("Apasat!"), tr("Succes"));
}