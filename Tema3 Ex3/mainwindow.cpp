#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QPlainTextEdit>
#include <QVBoxLayout> 
#include <QFileDialog>
#include <fstream>
#include<qmessagebox.h>
#include<qtextstream.h>
#include<qregexp.h>
#include<qfile.h>


MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
	
	// draw the editor component on the central widget
	//editor = new QPlainTextEdit(ui->centralWidget);
	QVBoxLayout *layout = new QVBoxLayout;
	
	//add the editor to the vertical layout
	//layout->addWidget(editor);
	// apply the layout on the central widget
	ui->centralWidget->setLayout(layout);
	ui->New->setTabsClosable(true);

	plainTextEdit = new QPlainTextEdit(ui->New);
	ui->New->insertTab(1,plainTextEdit, "New.txt");
	
	connect(ui->actionSave_As, SIGNAL(triggered(bool)),
		this, SLOT(saveAsFile(bool)));
	connect(ui->actionOpen, SIGNAL(triggered(bool)),
		this, SLOT(openFile(bool)));
	connect(ui->actionSave, SIGNAL(triggered(bool)), 
		this, SLOT(save(bool)));
	connect(ui->actionNew, SIGNAL(triggered(bool)),
		this, SLOT(newFile(bool)));
	connect(ui->New, SIGNAL(tabCloseRequested(int)), 
		this, SLOT(closeTab(int)));
	connect(ui->actionHelp, SIGNAL(triggered(bool)),
		this, SLOT(help()));
}

MainWindow::~MainWindow()
{
    delete ui;
}
void MainWindow::closeTab(const int& index)
{
	
	if (index == -1) {
		return;
	}

	QWidget* tabItem = ui->New->widget(index);
	// Removes the tab at position index from this stack of widgets.
	// The page widget itself is not deleted.
	ui->New->removeTab(index);

	delete(tabItem);
	tabItem = nullptr;
}

void MainWindow::help()
{
	
	QMessageBox::information(0,"We are eager to learn!","BUG BUSTERS!");
	
}



bool MainWindow::save(bool)
{
	QString ceva = ui->New->windowFilePath();
	QFileInfo FileMetaData(ceva);
	QString q = FileMetaData.fileName();
	QFile curFile(ceva);

	if (curFile.exists()!=true) 
	{
		saveFile(q);
	}
	else
	{
		saveAsFile(true);
	}
	return true;
}

bool MainWindow::saveFile(const QString &fileName)
{
	
	std::ofstream out(fileName.toStdString());

	out << plainTextEdit->toPlainText().toStdString();
	
	setCurrentFile(fileName);
	statusBar()->showMessage(tr("File saved"), 2000);
	return true;

}

void MainWindow::setCurrentFile(const QString &fileName)
{
	 
	plainTextEdit->document()->setModified(false);
	setWindowModified(false);

	QString shownName = fileName;
	if (fileName.isEmpty())
		shownName = "untitled.txt";
	setWindowFilePath(shownName);
}

void MainWindow::openFile(bool)
{
	QString FilePath;
	FilePath = QFileDialog::getOpenFileName(this, "Open File", "./", "Text files (*.txt)");
	QFile GetFile(FilePath);
	QFileInfo FileMetaData(FilePath);

	if (!GetFile.open(QIODevice::ReadOnly | QIODevice::Text))
	{
		QMessageBox::information(0, "ERROR", "Cannot open this file.");
	}
	else
	{
		
		QTextStream InputData(&GetFile);
		plainTextEdit = new QPlainTextEdit;
		ui->New->insertTab(clickTimes, plainTextEdit, FileMetaData.fileName());
		ui->New->setCurrentIndex(clickTimes);
		plainTextEdit->setPlainText(InputData.readAll());
		clickTimes++;
		ui->New->setTabsClosable(true);
	}
}

void MainWindow::newFile(bool)
{
	
	QString text = "New ";
	QString parse = QString::number(nou);
	plainTextEdit = new QPlainTextEdit(ui->New);
	ui->New->insertTab(clickTimes, plainTextEdit, text + parse + ".txt");
	clickTimes++;
	nou++;
}



void MainWindow::saveAsFile(bool)
{
	QString fileName = QFileDialog::getSaveFileName(this, tr("Save File"),
		"",
		tr("Text files (*.txt)"));
	QFileInfo FileMetaData(fileName);
	QString q = FileMetaData.fileName();

	std::ofstream out(fileName.toStdString());
	ui->New->setTabText(ui->New->currentIndex(),q);
	out << plainTextEdit->toPlainText().toStdString();
}
