#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QPlainTextEdit>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();
	
	public slots:
	void saveAsFile(bool);
	bool save(bool);
	bool saveFile(const QString &fileName);
	void setCurrentFile(const QString &fileName);
	void openFile(bool);
	void newFile(bool);
	void closeTab(const int & index);
	void help();
private:
    Ui::MainWindow *ui;
	QPlainTextEdit * plainTextEdit;
	int clickTimes = 1;
	int nou = 1;
};

#endif // MAINWINDOW_H
